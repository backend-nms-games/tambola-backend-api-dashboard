<?php
class Events{   
    
    private $itemsTable = "theme_events";      
    public $event_id;
    public $theme_id;
    public $event_name; 
    private $conn;
    public $current_time; 
    public $current_date; 
	
    public function __construct($db){
        $this->conn = $db;
    }	
    
    //check event or updated status
	function checkEvent(){
        date_default_timezone_set('Asia/Kolkata'); 
        $this->current_date = date('Y-m-d');
        $this->current_time = date('H:i:s');
        $stmt = $this->conn->prepare("SELECT * FROM ".$this->itemsTable." WHERE date(event_date) <= ? AND time(event_time) <= ?");
        $stmt->bind_param("ss", $this->current_date,$this->current_time);	 	
		$stmt->execute();			
		$result = $stmt->get_result(); 

        if($result->num_rows>0){
            $stmt = $this->conn->prepare("UPDATE ".$this->itemsTable." SET is_expired=1 WHERE date(event_date) <= ? AND time(event_time) <= ?"); 
            $stmt->bind_param("ss", $this->current_date,$this->current_time);
            if($stmt->execute()){
                return true;
            } 
        }	
		return $result;	
	}
    function readById($evenId){ 
        $this->user_id = $evenId;
        $stmt = $this->conn->prepare("SELECT * FROM ".$this->itemsTable." LEFT JOIN users ON users.user_id =theme_events.user_id WHERE theme_events.user_id = ?");
        $stmt->bind_param("i", $this->user_id);	 	
		$stmt->execute();			
		$result = $stmt->get_result();		
		return $result;	
	}
	function read(){	
		if($this->theme_id) {
			$stmt = $this->conn->prepare("SELECT * FROM ".$this->itemsTable." WHERE theme_id = ?");
			$stmt->bind_param("i", $this->theme_id);					
		} else {
			$stmt = $this->conn->prepare("SELECT * FROM ".$this->itemsTable);		
		}		
		$stmt->execute();			
		$result = $stmt->get_result();		
		return $result;	
	}
	
	 
}
?>