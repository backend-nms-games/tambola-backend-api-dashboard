<?php
class Users{   
    
    private $itemsTable = "users";   
    private $conn;
	
    public function __construct($db){
        $this->conn = $db;
    }	

    function selectUser($user){
         
        $stmt = $this->conn->prepare("SELECT * FROM ".$this->itemsTable." WHERE email = ?");
        $stmt->bind_param("i", $user); 		
		$stmt->execute();			
		$result = $stmt->get_result();		
		return $result;
    } 
	 
}
?>