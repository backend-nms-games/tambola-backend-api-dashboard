<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8"); 
include_once '../../config.php';
include_once '../../Class/Events.php'; 
$database = new Database();
$db = $database->getConnection();

$status = 404;
$message= null;
$items = new Events($db);


$now = new DateTime();

 
$items->checkEvent();
if(isset($_GET['userId'])  && !empty($_GET['userId'])){
    $result = $items->readById($_GET['userId']); 
    $itemRecords=array();
    $date = date('Y-m-d');
    $time = date('H:i:s');
    
    if($result->num_rows > 0){     
        $itemRecords=array();  
        while ($item = $result->fetch_assoc()) { 	
            extract($item); 
            $itemDetails=array(
                "user_id" =>  $user_id,
                "username" => $username, 
                "event_id" => $event_id,  		
                "ticket" => $tickets, 
                "ticket" => $tickets, 
            ); 
           array_push($itemRecords, $itemDetails);
        }  
        $status = 404;
        $message= "Data not found";
    }else{     
         $status = 404;
         $message= "Data not found";
        
    } 
    http_response_code(200);     
    echo json_encode(['status'=>$status,"message"=>$message,'items'=>$itemRecords]);
    exit;
}
